document.addEventListener("click", function(){
    document.getElementById("equals").addEventListener("click", function(){
        var which_op = null;
        var operations = document.getElementsByName("operation");
        for(var i=0; i<operations.length; i++){
            if(operations[i].checked){
                which_op = operations[i].value;
                break;
            }
        }
        var num1 = document.getElementById("num1").value;
        var num2 = document.getElementById("num2").value;
        switch(which_op) {
        case "add":
            document.getElementById("solution").textContent = parseFloat(num1) + parseFloat(num2);
            break;
        case "sub":
            document.getElementById("solution").textContent = parseFloat(num1) - parseFloat(num2);
            break;
        case "mul":
            document.getElementById("solution").textContent = parseFloat(num1) * parseFloat(num2);
            break;
        case "div":
            if (parseFloat(num2) == 0) {
                document.getElementById("solution").textContent = "Divide by 0 error";
            }
            else{
                document.getElementById("solution").textContent = parseFloat(num1) / parseFloat(num2);
            }
            break;
        default:
            document.getElementById("solution").textContent = "Select an operation";
            break;
        }
    }, false);
}, false);
